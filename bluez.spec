Name:             bluez
Summary:          Bluetooth utilities
Version:          5.54
Release:          13
License:          GPLv2+
URL:              http://www.bluez.org/
Source0:          http://www.kernel.org/pub/linux/bluetooth/bluez-%{version}.tar.xz
# The following sources all come from upstream
Source1:          bluez.gitignore
Source2:          69-btattach-bcm.rules
Source3:          btattach-bcm@.service
Source4:          btattach-bcm-service.sh

Patch0001:        0001-obex-Use-GLib-helper-function-to-manipulate-paths.patch
Patch0002:        0001-build-Always-define-confdir-and-statedir.patch
Patch0003:        0002-systemd-Add-PrivateTmp-and-NoNewPrivileges-options.patch
Patch0004:        0003-systemd-Add-more-filesystem-lockdown.patch
Patch0005:        0004-systemd-More-lockdown.patch
Patch0006:        backport-CVE-2021-3588.patch
# workaround for broken tests (reported upstream but not yet fixed)
# upstream:https://github.com/bluez/bluez/commit/5fc60b2ce7c4370ff8d9bc3d3c3434b212465f40
Patch0007:        backport-bluez-disable-test-mesh-crypto.patch
Patch0008:        backport-media-rename-local-function-conflicting-with-pause-2.patch
Patch6000:        backport-CVE-2020-27153.patch
Patch6001:        backport-0001-CVE-2021-3658.patch
Patch6002:        backport-0002-CVE-2021-3658.patch
Patch6003:        backport-CVE-2021-43400.patch
Patch6004:        backport-0001-CVE-2021-0129.patch
Patch6005:        backport-0002-CVE-2021-0129.patch
Patch6006:        backport-0003-CVE-2021-0129.patch
Patch6007:        backport-0004-CVE-2021-0129.patch
Patch6008:        backport-CVE-2022-0204.patch

BuildRequires:    dbus-devel >= 1.6 libell-devel >= 0.28 autoconf
BuildRequires:    glib2-devel libical-devel readline-devel 
BuildRequires:    json-c-devel systemd-devel cups-devel libtool automake 
Requires:         dbus >= 1.6 %{name}-libs = %{version}-%{release}
Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd
Provides:         bluez-hid2hci = %{version}-%{release} 
Provides:	  bluez-obexd = %{version}-%{release}
Provides:  	  bluez-mesh = %{version}-%{release}
Obsoletes:        bluez-hid2hci < %{version}-%{release}
Obsoletes:	  bluez-obexd < %{version}-%{release}
Obsoletes:	  bluez-mesh < %{version}-%{release}

%description
This package provides all utilities for use in Bluetooth applications.
The BLUETOOTH trademarks are owned by Bluetooth SIG, Inc., U.S.A.

%package          libs
Summary:          Libraries for bluez

%description      libs
Libraries forbluez.

%package          devel
Summary:          Development libraries for Bluetooth applications
Requires:         %{name}-libs = %{version}-%{release}
Provides:         bluez-libs-devel = %{version}-%{release}
Obsoletes:        bluez-libs-devel < %{version}-%{release}

%description      devel
This package provides development libraries and headers for Bluetooth related
applications.

%package_help

%package          cups
Summary: CUPS printer backend for Bluetooth printers
Requires: bluez%{?_isa} = %{version}-%{release}
Requires: cups

%description      cups
This package contains the CUPS backend

%prep
%autosetup -p1

%build
libtoolize -f
autoreconf -f -i
%configure --enable-tools --enable-library --enable-deprecated \
           --enable-sixaxis --enable-cups --enable-nfc --enable-mesh \
	   --enable-testing \
           --with-systemdsystemunitdir=%{_unitdir} \
           --with-systemduserunitdir=%{_userunitdir}
%make_build V=1

%install
%make_install
install -m0755 attrib/gatttool $RPM_BUILD_ROOT%{_bindir}

%delete_la

# Remove the cups backend from libdir, and install it in new default CUPS binary directory
if test -d ${RPM_BUILD_ROOT}/usr/lib64/cups ; then
        install -D -m0755 ${RPM_BUILD_ROOT}/usr/lib64/cups/backend/bluetooth ${RPM_BUILD_ROOT}%_cups_serverbin/backend/bluetooth
        rm -rf ${RPM_BUILD_ROOT}%{_libdir}/cups
fi

rm -f %{buildroot}/%{_sysconfdir}/udev/*.rules %{buildroot}/usr/lib/udev/rules.d/*.rules
install -D -p -m 0644 tools/hid2hci.rules %{buildroot}/%{_udevrulesdir}/97-hid2hci.rules
install -d -m 0755 %{buildroot}/%{_localstatedir}/lib/bluetooth
install -d -m 0755 %{buildroot}/%{_localstatedir}/lib/bluetooth/mesh
install -d %{buildroot}/%{_libdir}/bluetooth/

# Copy bluetooth config file
install -D -p -m0644 src/main.conf %{buildroot}/etc/bluetooth/main.conf
install -D -p -m0644 mesh/mesh-main.conf %{buildroot}/etc/bluetooth/mesh-main.conf

# Setup auto enable
sed -i 's/#\[Policy\]$/\[Policy\]/; s/#AutoEnable=false/AutoEnable=true/' ${RPM_BUILD_ROOT}/%{_sysconfdir}/bluetooth/main.conf

# Serial port connected Broadcom HCIs scripts
install -D -p -m0644 %{SOURCE2} ${RPM_BUILD_ROOT}/%{_udevrulesdir}/
install -D -p -m0644 %{SOURCE3} ${RPM_BUILD_ROOT}/%{_unitdir}/
install -D -p -m0755 %{SOURCE4} ${RPM_BUILD_ROOT}/%{_libexecdir}/bluetooth/

%check
make check

%ldconfig_scriptlets libs

%preun
%systemd_preun bluetooth.service
%systemd_user_preun obex.service
%systemd_preun bluetooth-mesh.service

%post
%systemd_post bluetooth.service
/sbin/udevadm trigger --subsystem-match=usb
%systemd_user_post obex.service
%systemd_post bluetooth-mesh.service

%postun
%systemd_postun_with_restart bluetooth.service


%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog
%license COPYING
%config %{_sysconfdir}/bluetooth/main.conf
%config %{_sysconfdir}/bluetooth/mesh-main.conf
%config %{_sysconfdir}/dbus-1/system.d/bluetooth.conf
%config %{_sysconfdir}/dbus-1/system.d/bluetooth-mesh.conf
%{_bindir}/*
%{_libdir}/bluetooth/
%{_prefix}/lib/udev/hid2hci
%{_libexecdir}/bluetooth/obexd
%{_libexecdir}/bluetooth/bluetoothd
%{_libexecdir}/bluetooth/bluetooth-meshd
%{_libexecdir}/bluetooth/btattach-bcm-service.sh
%{_localstatedir}/lib/bluetooth
%{_unitdir}/bluetooth.service
%{_unitdir}/btattach-bcm@.service
%{_unitdir}/bluetooth-mesh.service
%{_datadir}/zsh/site-functions/_bluetoothctl
%{_datadir}/dbus-1/services/org.bluez.obex.service
%{_datadir}/dbus-1/system-services/org.bluez.service
%{_datadir}/dbus-1/system-services/org.bluez.mesh.service
%{_userunitdir}/obex.service
%{_udevrulesdir}/97-hid2hci.rules
%{_udevrulesdir}/69-btattach-bcm.rules

%files            libs
%defattr(-,root,root)
%{_libdir}/libbluetooth.so.*

%files            devel
%defattr(-,root,root)
%doc doc/*txt
%{_libdir}/libbluetooth.so
%{_includedir}/bluetooth
%{_libdir}/pkgconfig/bluez.pc

%files            cups
%defattr(-,root,root)
%_cups_serverbin/backend/bluetooth

%files            help
%defattr(-,root,root)
%{_mandir}/man1/*
%{_mandir}/man8/*

%changelog
* Fri Mar 18 2022 xingxing <xingxing9@h-partners.com> - 5.54-9
- Type:CVE
- CVE:CVE-2022-0204
- SUG:NA
- DESC:fix CVE-2022-0204 and add prefix for CVE-2021-0129

* Tue Mar 1 2022 xingxing <xingxing9@h-partners.com> - 5.54-12
- Type:CVE
- CVE:CVE-2021-0129
- SUG:NA
- DESC:fix CVE-2021-0129

* Fri Feb 11 2022 xingxing <xingxing9@h-partners.com> - 5.54-11
- Type:CVE
- CVE:CVE-2021-43400
- SUG:NA
- DESC:fix CVE-2021-43400

* Thu Jan 13 2022 wangkerong <wangkerong@huawei.com> - 5.54-10
- DESC:fix an error in logs during bluez installation or uninstallation

* Fri Sep 24 2021 yanan <yanan@huawei.com> - 5.54-9
- DESC:fix CVE-2021-3658

* Tue Aug 10 2021 zhanzhimin <zhanzhimin@huawei.com> - 5.54-8
- Type:CVE
- ID:CVE-2020-27153
- SUG:NA
- DESC:fix CVE-2020-27153

* Tue Aug 10 2021 yanan <yanan@huawei.com> - 5.54-7
- DESC: fix conflict with unistd.h 

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 5.54-6
- DESC: delete -S git from %autosetup, and delete BuildRequires git-core

* Mon Jun 28 2021 hanhui <hanhui15@huawei.com> - 5.54-5
- DESC:disable test-mesh-crypto

* Sat Jun 26 2021 zhanzhimin <zhanzhimin@huawei.com> - 5.54-4
- Type:CVE
- ID:CVE-2021-3588
- SUG:NA
- DESC:fix CVE-2021-3588

* Wed Sep 16 2020 orange-snn <songnannan2@huawei.com> - 5.54-3
- bugfix test-mesh-crypto faild

* Mon Aug 10 2020 shixuantong <shixuantong@huawei.com> - 5.54-2
- update yaml file

* Wed Jul 22 2020 songnannan <songnannan2@huawei.com> - 5.54-1
- update to 5.54

* Wed May 20 2020 songnannan <songnannan2@huawei.com> - 5.50-9
- delete the check temporarily

* Wed Apr 22 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.50-8
- Type:cves
- ID:CVE-2020-0556
- SUG:NA
- DESC:fix CVE-2020-0556

* Wed Mar 18 2020 chenzhen <chenzhen44@huawei.com> - 5.50-7
- Type:cves
- ID:CVE-2018-10910
- SUG:NA
- DESC:fix CVE-2018-10910

* Mon Feb 17 2020 hexiujun <hexiujun1@huawei.com> - 5.50-6
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:unpack libs subpackage

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.50-5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: delete patches

* Tue Sep 17 2019 Alex Chao <zhaolei746@huawei.com> - 5.50-4
- Package init
